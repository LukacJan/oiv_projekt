function kviz3_11(){
    event.preventDefault();
    if(document.getElementById("ch3").checked && !document.getElementById("ch2").checked && !document.getElementById("ch1").checked){
        if (sessionStorage.getItem('ponovitev1') == null) {
            sessionStorage.setItem('tocke', 10);
        }
        window.location.href = "kviz_3_1.2.html";
    } else {
        sessionStorage.setItem('tocke', 9);
        sessionStorage.setItem('ponovitev1', true);
        document.getElementById("skrito").style.display = "block";

    }
}

function kviz3_12(){
    event.preventDefault();
    if(document.getElementById("ch1").checked && !document.getElementById("ch2").checked && !document.getElementById("ch3").checked){
        window.location.href = "kviz_3_1.3.html";
    } else {
        if (sessionStorage.getItem('ponovitev2') == null) {
            sessionStorage.setItem('tocke', sessionStorage.getItem('tocke') - 1);
            sessionStorage.setItem('ponovitev2', true);
        }
        document.getElementById("skrito").style.display = "block";

    }
}

function kviz3_13(){
    event.preventDefault();
    if(document.getElementById("ch2").checked && !document.getElementById("ch1").checked && !document.getElementById("ch3").checked){
        window.location.href = "kviz_3_1.4.html";
    } else {
        if (sessionStorage.getItem('ponovitev3') == null) {
            sessionStorage.setItem('tocke', sessionStorage.getItem('tocke') - 1);
            sessionStorage.setItem('ponovitev3', true);
        }
        document.getElementById("skrito").style.display = "block";

    }
}

function kviz3_14(){
    event.preventDefault();
    if(document.getElementById("ch1").checked && !document.getElementById("ch3").checked && !document.getElementById("ch2").checked){
        window.location.href = "kviz_3_1.5.html";
    } else {
        if (sessionStorage.getItem('ponovitev4') == null) {
            sessionStorage.setItem('tocke', sessionStorage.getItem('tocke') - 1);
            sessionStorage.setItem('ponovitev4', true);
        }
        document.getElementById("skrito").style.display = "block";

    }
}

function kviz3_15(){
    event.preventDefault();
    if(document.getElementById("ch2").checked && !document.getElementById("ch1").checked && !document.getElementById("ch3").checked){
        window.location.href = "kviz_3_2.1.html";
    } else {
        if (sessionStorage.getItem('ponovitev5') == null) {
            sessionStorage.setItem('tocke', sessionStorage.getItem('tocke') - 1);
            sessionStorage.setItem('ponovitev5', true);
        }
        document.getElementById("skrito").style.display = "block";

    }
}

function kviz3_1(){
    event.preventDefault();
    if(document.getElementById("ch1").checked && document.getElementById("ch2").checked && document.getElementById("ch3").checked){
        window.location.href = "kviz_3_2.html";
    } else {
        if (sessionStorage.getItem('ponovitev6') == null) {
            sessionStorage.setItem('tocke', sessionStorage.getItem('tocke') - 1);
            sessionStorage.setItem('ponovitev6', true);
        }
        document.getElementById("skrito").style.display = "block";

    }
}

function kviz3_2(){
    event.preventDefault();
    if(document.getElementById("ch2").checked && !document.getElementById("ch1").checked && !document.getElementById("ch3").checked){
        window.location.href = "kviz_3_3.html";
    } else {
        if (sessionStorage.getItem('ponovitev7') == null) {
            sessionStorage.setItem('tocke', sessionStorage.getItem('tocke') - 1);
            sessionStorage.setItem('ponovitev7', true);
        }
        document.getElementById("skrito").style.display = "block";
    }
}


function kviz3_3(){
    event.preventDefault();
    if(document.getElementById("ch2").checked && !document.getElementById("ch1").checked && !document.getElementById("ch3").checked){
        window.location.href="kviz_3_4.html";
    }else{
        if (sessionStorage.getItem('ponovitev8') == null) {
            sessionStorage.setItem('tocke', sessionStorage.getItem('tocke') - 1);
            sessionStorage.setItem('ponovitev8', true);
        }
        document.getElementById("skrito").style.display="block";
    }
}

function kviz3_4(){
    event.preventDefault();
    if (document.getElementById("text1").value == "{{5*5}}" || document.getElementById("text1").value == "{{5+5}}" || document.getElementById("text1").value == "{{5-5}}" || 
    document.getElementById("text1").value == "${5*5}" || document.getElementById("text1").value == "${5+5}" || document.getElementById("text1").value == "${5-5}" || 
    document.getElementById("text1").value == "${{5*5}}" || document.getElementById("text1").value == "${{5+5}}" || document.getElementById("text1").value == "${{5-5}}" ){
        window.location.href="kviz_3_5.html";
    }else{
        if (sessionStorage.getItem('ponovitev9') == null) {
            sessionStorage.setItem('tocke', sessionStorage.getItem('tocke') - 1);
            sessionStorage.setItem('ponovitev9', true);
        }
        document.getElementById("skrito").style.display="block";
    }
}

function kviz3_5(){
    event.preventDefault();
    if(document.getElementById("ch1").checked && !document.getElementById("ch2").checked && !document.getElementById("ch3").checked && !document.getElementById("ch4").checked){
        window.location.href="kviz_3_6.html";
    }else{
        if (sessionStorage.getItem('ponovitev10') == null) {
            sessionStorage.setItem('tocke', sessionStorage.getItem('tocke') - 1);
            sessionStorage.setItem('ponovitev10', true);
        }
        document.getElementById("skrito").style.display="block";
    }
}

function namig(){
    document.getElementById("namig").style.display = 'block';
    return false;
}



function rez() {
    event.preventDefault();

    document.getElementById("rezultat").innerText = "Vaše končno število točk je: " + sessionStorage.getItem('tocke') + "/10";
    if (sessionStorage.getItem('tocke') >= 6) {
        document.getElementById("opis").innerText = "Na kvizu ste se odrezali dobro. Poglobite svoje znanje s pomočjo uporabnih povezav." +
        "z obiskom izbranih virov.";
        document.getElementById("poglobljeno").style.display = 'block';
    }else if(sessionStorage.getItem('tocke') < 6) {
        document.getElementById("opis1").innerText = "Kviz ste reševali bolj slabo. Predlagamo da si izboljšate vaše osnovno znanje " +
        "z obiskom izbranih virov.";
        document.getElementById("slabo").style.display = 'block';
    }
}