function namig1() {
    document.getElementById("namig_skrit_1").style.display = 'block';
    return false;
}

function skrij1() {
    document.getElementById("namig_skrit_1").style.display = 'none';
    return false;
}

function skrij_err1() {
    document.getElementById("err_skrit_1").style.display = 'none';
    return false;
}

function vpr_1() {
    event.preventDefault();
    if (document.getElementById("rad_osnova").checked) {
        if (sessionStorage.getItem('ponovitev1') == null) {
            sessionStorage.setItem('tocke', 10);
        }
        window.location.href = "kviz_2_2.html";
    } else {
        sessionStorage.setItem('tocke', 9);
        sessionStorage.setItem('ponovitev1', true);
        document.getElementById("err_skrit_1").style.display = 'block';
        console.log("napaka");
    }
}

function vpr_2() {
    event.preventDefault();
    if (document.getElementById("ch1").checked && document.getElementById("ch4").checked && !document.getElementById("ch2").checked &&
        !document.getElementById("ch3").checked && !document.getElementById("ch5").checked) {
        window.location.href = "kviz_2_3.html";
    } else {
        if (sessionStorage.getItem('ponovitev2') == null) {
            sessionStorage.setItem('tocke', sessionStorage.getItem('tocke') - 1);
            sessionStorage.setItem('ponovitev2', true);
        }
        document.getElementById("err_skrit_1").style.display = 'block';
        console.log("napaka");
    }
}

function vpr_3() {
    event.preventDefault();
    if (document.getElementById("ch1").checked && document.getElementById("ch2").checked && document.getElementById("ch5").checked &&
        !document.getElementById("ch3").checked && !document.getElementById("ch4").checked) {
        window.location.href = "kviz_2_4.html";
    } else {
        if (sessionStorage.getItem('ponovitev3') == null) {
            sessionStorage.setItem('tocke', sessionStorage.getItem('tocke') - 1);
            sessionStorage.setItem('ponovitev3', true);
        }
        document.getElementById("err_skrit_1").style.display = 'block';
        console.log("napaka");
    }
}

function vpr_4() {
    event.preventDefault();
    if (document.getElementById("ch1").checked && document.getElementById("ch3").checked && document.getElementById("ch5").checked &&
        !document.getElementById("ch2").checked && !document.getElementById("ch4").checked && !document.getElementById("ch6").checked) {
        window.location.href = "kviz_2_5.html";
    } else {
        if (sessionStorage.getItem('ponovitev4') == null) {
            sessionStorage.setItem('tocke', sessionStorage.getItem('tocke') - 1);
            sessionStorage.setItem('ponovitev4', true);
        }
        document.getElementById("err_skrit_1").style.display = 'block';
        console.log("napaka");
    }
}

function vpr_5() {
    event.preventDefault();
    if (document.getElementById("ch1").checked && document.getElementById("ch2").checked && document.getElementById("ch3").checked && document.getElementById("ch4").checked) {
        window.location.href = "kviz_2_6.html";
    } else {
        if (sessionStorage.getItem('ponovitev5') == null) {
            sessionStorage.setItem('tocke', sessionStorage.getItem('tocke') - 1);
            sessionStorage.setItem('ponovitev5', true);
        }
        document.getElementById("err_skrit_1").style.display = 'block';
        console.log("napaka");
    }
}

function vpr_6() {
    event.preventDefault();
    if (document.getElementById("ch1").checked && document.getElementById("ch3").checked && document.getElementById("ch6").checked &&
        !document.getElementById("ch2").checked && !document.getElementById("ch4").checked && !document.getElementById("ch5").checked) {
        window.location.href = "kviz_2_7.html";
    } else {
        if (sessionStorage.getItem('ponovitev6') == null) {
            sessionStorage.setItem('tocke', sessionStorage.getItem('tocke') - 1);
            sessionStorage.setItem('ponovitev6', true);
        }
        document.getElementById("err_skrit_1").style.display = 'block';
        console.log("napaka");
    }
}

function vpr_7() {
    event.preventDefault();
    if (document.getElementById("ch1").checked && document.getElementById("ch3").checked &&
        document.getElementById("ch5").checked && !document.getElementById("ch2").checked &&
        !document.getElementById("ch4").checked) {
        window.location.href = "kviz_2_8.html";
    } else {
        if (sessionStorage.getItem('ponovitev7') == null) {
            sessionStorage.setItem('tocke', sessionStorage.getItem('tocke') - 1);
            sessionStorage.setItem('ponovitev7', true);
        }
        document.getElementById("err_skrit_1").style.display = 'block';
        console.log("napaka");
    }
}

function vpr_8() {
    event.preventDefault();
    if (document.getElementById("ch1").checked && document.getElementById("ch2").checked &&
        document.getElementById("ch3").checked && document.getElementById("ch4").checked &&
        document.getElementById("ch5").checked) {
        window.location.href = "kviz_2_9.html";
    } else {
        if (sessionStorage.getItem('ponovitev8') == null) {
            sessionStorage.setItem('tocke', sessionStorage.getItem('tocke') - 1);
            sessionStorage.setItem('ponovitev8', true);
        }
        document.getElementById("err_skrit_1").style.display = 'block';
        console.log("napaka");
    }
}

function vpr_9() {
    event.preventDefault();
    if (document.getElementById("rad_rsa").checked) {
        window.location.href = "kviz_2_10.html";
    } else {
        if (sessionStorage.getItem('ponovitev9') == null) {
            sessionStorage.setItem('tocke', sessionStorage.getItem('tocke') - 1);
            sessionStorage.setItem('ponovitev9', true);
        }
        document.getElementById("err_skrit_1").style.display = 'block';
        console.log("napaka");
    }
}

function vpr_10() {
    event.preventDefault();
    if (document.getElementById("ch2").checked && document.getElementById("ch3").checked && !document.getElementById("ch1").checked) {
        window.location.href = "kviz_2_konec.html";
    } else {
        if (sessionStorage.getItem('ponovitev10') == null) {
            sessionStorage.setItem('tocke', sessionStorage.getItem('tocke') - 1);
            sessionStorage.setItem('ponovitev10', true);
        }
        document.getElementById("err_skrit_1").style.display = 'block';
        console.log("napaka");
    }
}

function rez() {
    event.preventDefault();

    //sessionStorage.setItem('tocke', 9);

    document.getElementById("rezultat").innerText = "Vaše končno število točk je: " + sessionStorage.getItem('tocke') + "/10";
    if (sessionStorage.getItem('tocke') >= 9) {
        document.getElementById("opis").innerText = "Na kvizu ste se odrezali izvrstno. Če želite lahko še poglobite svoje znanje" +
            " z obiskom izbranih virov.";
        document.getElementById("poglobljeno").style.display = 'block';
    } else if (sessionStorage.getItem('tocke') >= 6) {
        document.getElementById("opis1").innerText = "Kviz ste rešili zadovoljivo. Vendar pa predlagamo da si izboljšate vaše znanje" +
            " z obiskom izbranih virov.";
        document.getElementById("poglobljeno").style.display = 'block';
    } else if (sessionStorage.getItem('tocke') < 6) {
        document.getElementById("opis1").innerText = "Kviz ste reševali bolj slabo. Predlagamo da si izboljšate vaše osnovno znanje" +
            " z obiskom izbranih virov.";
        document.getElementById("poglobljeno").style.display = 'block';
    }
}